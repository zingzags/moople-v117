package constants;

import client.MapleCharacter;
import constants.skills.Aran;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author kevintjuh93
 */
public class GameConstants {
   
    
    public static int getNumSteal(int jobNum){
        switch (jobNum){
            case 1:
                return 4;
            case 2:
                return 4;
            case 3:
                return 3;
            case 4:
                return 2;
        }
        return 0;
    }
    
    private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public static int getCurrentDate() {    
        String time = date.format(Calendar.getInstance().getTime()); 
        return Integer.parseInt(new StringBuilder(time.substring(0, 4)).append(time.substring(5, 7)).append(time.substring(8, 10)).toString());
    }    
    
    public static int getHiddenSkill(int skill) {
        switch (skill) {
            case Aran.HIDDEN_FULL_DOUBLE:
            case Aran.HIDDEN_FULL_TRIPLE:
                return Aran.FULL_SWING;
            case Aran.HIDDEN_OVER_DOUBLE:
            case Aran.HIDDEN_OVER_TRIPLE:
                return Aran.OVER_SWING;
        }
        return skill;
    }

    
    
    
    public static boolean isKOC(int job){
        return job >= 1000 && job < 2000;
    }

    public static boolean isEvan(int job){
        return job == 2001 || (job / 100 == 22);
    }

    public static boolean isMercedes(int job){
        return job == 2002 || (job / 100 == 23);
    }
	
    public static boolean isJett(int job){
        return job == 508 || (job / 10 == 57);
    }
    
    public static boolean isPaladin(int job){
        return job == 122;
    }
    
    public static boolean isPhantom(int job){     
	return job == 2003 || job / 100 == 24;
    }
        
    public static boolean isWildHunter(int job){     
	 return job == 3000 || job >= 3300 && job <= 3312;
    }

    public static boolean isSeparatedSp(int job){
         return isDualBlade(job) || isEvan(job) || isResist(job) || isMercedes(job) || isJett(job) || isPhantom(job) || isMihile(job);
    }
     
    public static boolean isDemon(int job){
        return job == 3001 || job >= 3100 && job <= 3112;
    }

    public static boolean isLuminous(final int job) {
        return job == 2004 || job >= 2700 && job <= 2712;
    }

    public static boolean isKaiser(final int job) {
        return job == 6000 || job >= 6100 && job <= 6112;
    }

    public static boolean isAngelicBuster(final int job) {
        return job == 6001 || job >= 6500 && job <= 6512;
    }

    public static boolean isNova(final int job) {
        return job / 1000 == 6;
    }
    
    public static boolean isHayato(int job) {
        return job == 4001 || job >= 4100 && job <= 4112;
    }

    public static boolean isKanna(int job) {
        return job == 4002 || job >= 4200 && job <= 4212;
    }

    public static boolean isSengoku(int job) {
        return job / 1000 == 4;
    }
    
    public static boolean isZero(int job) {
        return job == 10000 || job >= 10100 && job <= 10112;
    }
    
    public static boolean isBeastTamer(int job) {
        return job == 11212;
    }
    
    public static boolean isMarkings(int job) {
        return isXenon(job) || isDemon(job) || isDemonAvenger(job) || isBeastTamer(job);
    }
    
    public static boolean isXenon(int job) {
        return (job == 3002) || (job >= 3600 && job <= 3612);
    }

    public static boolean isDemonAvenger(int job) {
        return (job == 3101) || (job >= 3120 && job <= 3122);
    }    
    
    public static boolean isAran(int job){
        return job >= 2000 && job <= 2112 && job != 2001 && job != 2002 && job != 2003;
    }

    public static boolean isResist(int job){
        return job / 1000 == 3;
    }

    public static boolean isAdventurer(int job){
        return job >= 0 && job < 1000;
    }

    public static boolean isCannon(int job){
        return job == 1 || job == 501 || (job >= 530 && job <= 532);
    }
    
    public static boolean isMageArcaneAim(int job){
        return job == 212 || job == 222 || job == 232;
    }
    
    public static boolean isDualBlade(int job){
        return (job >= 430 && job <= 434);
    }
    
    public static boolean isMihile(int job){
        return job == 5000 || (job >= 5100 && job <= 5112);
    }

    public static boolean isBeginnerJob(int job) {
        return job == 0 || job == 1 || job == 1000 || job == 2000 || job == 2001 || job == 3000 || job == 3001 || job == 2002 || job == 2003;
    }
    
    public static boolean isChair(int itemid){
        return itemid / 10000 == 301;
    }
      
    public static int getSkillBook(int job){
        if (job >= 2210 && job <= 2218){
            return job - 2209;
        }
        switch (job){
            case 570:
            case 2310:
            case 2410:
            case 3110:
            case 3210:
            case 3310:
            case 3510:
            case 5110:
                return 1;
            case 571:
            case 2311:
            case 2411:
            case 3111:
            case 3211:
            case 3311:
            case 3511:
            case 5111:
                return 2;
            case 572:
            case 2312:
            case 2412:
            case 3112:
            case 3212:
            case 3312:
            case 3512:
            case 5112:
                return 3;
        }
        return 0;
    }
 
    
public static int getSkillBook(int job, int level){
   if (job >= 2210 && job <= 2218){
            return job - 2209;
        }
        switch (job){
           
            case 508:
            case 570:
            case 571:
            case 572:
            case 2300:
            case 2310:
            case 2311:
            case 2312:
            case 2400:
            case 2410:
            case 2411:
            case 2412:
            case 3100:
            case 3200:
            case 3300:
            case 3500:
            case 3110:
            case 3210:
            case 3310:
            case 3510:
            case 3111:
            case 3211:
            case 3311:
            case 3511:
            case 3112:
            case 3212:
            case 3312:
            case 3512:
            case 5100:
            case 5110:
            case 5111:
            case 5112:
                return (level <= 30 ? 0 : (level >= 31 && level <= 70 ? 1 : (level >= 71 && level <= 120 ? 2 : (level >= 120 ? 3 : 0))));
        }
        return 0;
    }

    public static int getMountItem(int sourceid, MapleCharacter chr){

        switch (sourceid){
            case 20021160: 
                return 1932086; 
            case 20021161: 
                return 1932087; 
            case 20031160: 
                return 1932106; 
            case 20031161: 
                return 1932107; 
            case 5221006:
                return 1932000;
       /*     case 33001001: //temp.
                if (chr == null){
                    return 1932015;
                }
                switch (chr.getIntNoRecord(JAGUAR)){
                    case 20:
                        return 1932030;
                    case 30:
                        return 1932031;
                    case 40:
                        return 1932032;
                    case 50:
                        return 1932033;
                    case 60:
                        return 1932036;
                    case 70:
			return 1932100;
                }
                        return 1932015; */
              case 35001002:
              case 35120000:
                   return 1932016;
               case 30011109:
                    return 1932085;
        }
        /*
        if (!isBeginnerJob(sourceid / 10000)){
            if (sourceid / 10000 == 8000 && sourceid != 80001000){ //todoo clean up
                Skill skil = SkillFactory.getSkill(sourceid);
                if (skil != null && skil.getTamingMob() > 0){
                    return skil.getTamingMob();
                } else {
                    int link = getLinkedMountItem(sourceid);
                    if (link > 0){
                        if (link < 10000){
                            return getMountItem(link, chr);
                        } else {
                            return link;
                        }
                    }
                }
            }
            return 0;
        }*/
        switch (sourceid % 10000){
            case 1013:
            case 1046:
                return 1932001;
            case 1015:
            case 1048:
                return 1932002;
            case 1016:
            case 1017:
            case 1027:
                return 1932007;
            case 1018:
                return 1932003;
            case 1019:
                return 1932005;
            case 1025:
                return 1932006;
            case 1028:
                return 1932008;
            case 1029:
                return 1932009;
            case 1030:
                return 1932011;
            case 1031:
                return 1932010;
            case 1033:
                return 1932013;
            case 1034:
                return 1932014;
            case 1035:
                return 1932012;
            case 1036:
                return 1932017;
            case 1037:
                return 1932018;
            case 1038:
                return 1932019;
            case 1039:
                return 1932020;
            case 1040:
                return 1932021;
            case 1042:
                return 1932022;
            case 1044:
                return 1932023;
            case 1045:
                 return 1932030; //wth? helicopter? i didnt see one, so we use hog
            case 1049:
                return 1932025;
            case 1050:
                return 1932004;
            case 1051:
                return 1932026;
            case 1052:
                return 1932027;
            case 1053:
                return 1932028;
            case 1054:
                return 1932029;
            case 1063:
                return 1932034;
            case 1064:
                return 1932035;
            case 1065:
                return 1932037;
            case 1069:
                return 1932038;
            case 1070:
                return 1932039;
            case 1071:
                return 1932040;
            case 1072:
                return 1932041;
            case 1084:
                return 1932043;
            case 1089:
                return 1932044;
            case 1096:
                return 1932045;
            case 1101:
                return 1932046;
            case 1102:
                return 1932061;
            case 1106:
                return 1932048;
            case 1118:
                return 1932060;
            case 1115:
                return 1932052;
            case 1121:
                return 1932063;
            case 1122:
                return 1932064;
            case 1123:
                return 1932065;
            case 1128:
                return 1932066;
            case 1130:
                return 1932072;
            case 1136:
                return 1932078;
            case 1138:
                return 1932080;
            case 1139:
                return 1932081;
            //FLYING
            case 1143:
            case 1144:
            case 1145:
            case 1146:
            case 1147:
            case 1148:
            case 1149:
            case 1150:
            case 1151:
            case 1152:
            case 1153:
            case 1154:
            case 1155:
            case 1156:
            case 1157:
                return 1992000 + (sourceid % 10000) - 1143;
            default:
                return 0;
        }
    }    
    
    
}
