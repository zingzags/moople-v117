/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package constants;

import client.inventory.MapleInventoryType;

/**
 *
 * @author Jay Estrella
 */
public final class ItemConstants {    
    public final static int LOCK = 0x01;
    public final static int SPIKES = 0x02;
    public final static int COLD = 0x04;
    public final static int UNTRADEABLE = 0x08;
    public final static int KARMA = 0x10;
    public final static int CHARM_EQUIPPED = 0x20;
    public final static int ANDROID_EQUIPPED = 0x40;
    public final static int CRAFTED = 0x80;
    public final static int SHIELD_WARD = 0x100;
    public final static int LUCKY_DAY = 0x200;
    public final static int KARMA_USE = 0x400;
    public final static int KARMA_2 = 0x1000;
    public final static int SHEILD_SCROLL = 0x2000;
    public final static int GUARDIAN_SCROLL = 0x4000;
    public static final int PET_COME = 128;
    public static final int UNKNOWN_SKILL = 256;
    public final static float ITEM_ARMOR_EXP = 1 / 350000;
    public static final float ITEM_WEAPON_EXP = 1 / 700000;
    public static final byte HIDDEN = 1;
    public static final byte RARE = 17;
    public static final byte EPIC = 18;
    public static final byte UNIQUE = 19;
    public static final byte LEGENDARY = 20;

    public final static boolean EXPIRING_ITEMS = true;

    public static int getFlagByInt(int type) {
        if (type == 128) {
            return PET_COME;
        } else if (type == 256) {
            return UNKNOWN_SKILL;
        }
        return 0;
    }

    public static boolean isThrowingStar(int itemId) {
        return itemId / 10000 == 207;
    }

    public static boolean isBullet(int itemId) {
        return itemId / 10000 == 233;
    }
    
   public static boolean isMonsterBook(int itemId){
     return itemId == 1172000;
   }
   
    public static boolean isRechargable(int itemId) {
        return itemId / 10000 == 233 || itemId / 10000 == 207;
    }

    public static boolean isArrowForCrossBow(int itemId) {
        return itemId / 1000 == 2061;
    }

    public static boolean isArrowForBow(int itemId) {
        return itemId / 1000 == 2060;
    }

    public static boolean isPet(int itemId) {
        return itemId / 1000 == 5000;
    }
    
    public static boolean isMagicWeapon(int itemId) {
	return itemId / 10000 == 137 || itemId / 10000 == 138;
    }
    
    public static boolean isMechanicItem(int itemId){
        return itemId >= 1610000 && itemId < 1660000;
    }

    public static boolean isEvanDragonItem(int itemId){
        return itemId >= 1940000 && itemId < 1980000; //194 = mask, 195 = pendant, 196 = wings, 197 = tail
    }
    
    public static boolean isFamiliar(int itemId) {
        return itemId / 10000 == 287;
    }
 
    public static boolean isWeapon(int itemId){
        return itemId >= 1300000 && itemId < 1533000;
    }
    
    public static boolean isAccessory(int itemId){
        return itemId >= 1010000 && itemId < 1040000 || itemId >= 1122000 && itemId < 1153000 || itemId >= 1112000 && itemId < 1113000;
    }
    
    public static boolean isKatara(int itemId) {
        return itemId / 10000 == 134;
    }

    public static boolean isDagger(int itemId) {
        return itemId / 10000 == 133;
    }
    public static boolean isExtendedBag(int itemId){
        return itemId / 10000 == 433;
    }
    public static boolean InsideExtendedBag(int slot, byte type){
        return slot >= 101 && slot <= 512 && type == MapleInventoryType.ETC.getType();
    }
    
    public static MapleInventoryType getInventoryType(final int itemId) {
	final byte type = (byte) (itemId / 1000000);
	if (type < 1 || type > 5) {
	    return MapleInventoryType.UNDEFINED;
	}
	return MapleInventoryType.getByType(type);
    }
}
