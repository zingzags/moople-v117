/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package constants;

import client.JobFlag;

/**
 *
 * @author Steven
 */
public class MapleJobs {
    
    public static boolean jobActive = true;
    public static byte jobOrder = 0x11;
    
    public enum activeJobs {

        Resistance(JobFlag.ENABLED, 1),
        Adventurer(JobFlag.ENABLED, 1),
        Cygnus(JobFlag.ENABLED, 1),
        Aran(JobFlag.ENABLED, 1),
        Evan(JobFlag.ENABLED, 1),
        Mercedes(JobFlag.ENABLED, 1),
        Demon(JobFlag.ENABLED, 1),
        Phantom(JobFlag.ENABLED, 1),
        DualBlade(JobFlag.ENABLED, 1),
        Mihile(JobFlag.ENABLED, 1),
        Luminous(JobFlag.ENABLED, 1),
        Kaiser(JobFlag.ENABLED, 1),
        AngelicBuster(JobFlag.ENABLED, 1),
        Cannoneer(JobFlag.ENABLED, 1),
        Xenon(JobFlag.ENABLED, 1),
        Zero(JobFlag.ENABLED, 2),        
        Jett(JobFlag.ENABLED, 1),
        Hayato(JobFlag.ENABLED, 1),
        Kanna(JobFlag.ENABLED, 1),
        BeastTamer(JobFlag.ENABLED, 1),
        Shade(JobFlag.ENABLED, 1);
        
        private int flag;
        
        private int requirements;

        private activeJobs(JobFlag flag, int requirements) {
            this.flag = flag.getFlag();
            this.requirements = requirements;
        }

        public int getFlag() {
            return flag;
        }
        
        public int getRequirements() {
            return requirements;
        }
        
    }        
}
