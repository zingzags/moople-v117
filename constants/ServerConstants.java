/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package constants;

public class ServerConstants {
    public static short VERSION = 158;
    public static String PATCH = "2";
    public static boolean TESTING_SERVER = true; //The reason why I added this is because of issues with certain redirectors causing the session to not be "Logged in" in MapleClient
    public static String NAME = "MapleCode";
    public static String[] WORLD_NAMES = {"Scania", "Bera", "Broa", "Windia", "Khaini", "Bellocan", "Mardia", "Kradia", "Yellonde", "Demethos", "Galicia", "El Nido", "Zenith", "Arcenia", "Kastia", "Judis", "Plana", "Kalluna", "Stius", "Croa", "Medere"};
    public static int CHARACTER_LIMIT = 23;
    // Rate Configuration
    public static final byte QUEST_EXP_RATE = 1;
    public static final byte QUEST_MESO_RATE = 1;
    public static final int TRAIT_RATE = 1;
    // Login Configuration
    public static final int CHANNEL_LOAD = 150;//Players per channel
    public static final long RANKING_INTERVAL = 3600000;
    public static final boolean ENABLE_PIC = true;
    //Event Configuration
    public static final boolean PERFECT_PITCH = false;
    public static final String EVENTS = "automsg KerningPQ Boats Subway AirPlane elevator";
    // IP Configuration
    public static final String HOST = "127.0.0.1";
    //Database Configuration
    public static final String DB_URL = "jdbc:mysql://localhost:3306/MoopleDEV?autoReconnect=true";
    public static final String DB_USER = "root";
    public static final String DB_PASS = "";
    //Website
    public static final String WEB_URL = "";
    //Redirector
    public static final boolean REDIRECTOR = true;
    public static final int CHANNEL_PORT = 8586;
    public static final int PORT = 8484;
    public static final String NEXON_IP = "8.31.99.141";
}