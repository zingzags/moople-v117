package client;

import constants.ExpTable;
import constants.ServerConstants;
import tools.MaplePacketCreator;

/**
 *
 * @author Steven, even(RageZone), Zingzags(PokeCommunity)
 */
public class MapleTraits {
   
   public static enum MapleTrait{ 
    AMBITION(500, MapleStat.AMBITION),
    INSIGHT(500, MapleStat.INSIGHT),
    WILLPOWER(500, MapleStat.WILLPOWER),
    DILLIGENCE(500, MapleStat.DILLIGENCE),
    EMPATHY(500, MapleStat.EMPATHY),
    CHARM(5000, MapleStat.CHARM);
    
    private int dailyLimit;
    private MapleStat stat;
    
    MapleTrait(int dailyLimit, MapleStat stat){
        this.dailyLimit = dailyLimit;
        this.stat = stat;
    }
    
    public MapleStat getStat(){
        return stat;
    }
    
    public int getDailyLimit(){
        return dailyLimit;
    }    
    
    /*This method below is probably a check to see if the player has a certain 
     * level of trait, or is a quest related to the trait*/
    public static MapleTrait getByQuestName(String quest){
        String quest_1 = quest.substring(0, quest.length() - 3); //e.g. charmEXP, charmMin
        for (MapleTrait t : MapleTrait.values()){
             if (t.name().equals(quest_1)){
               return t;          
             }
       }
       return null;
     }    
   }
   
    private MapleTrait traitType;
    private int totalExp = 0, localTotalExp = 0, dailyExp = 0, traitLevel = 0, expirence = 0;
    
    public MapleTraits(MapleTrait type){
        traitType = type;
    }
    
    public MapleTrait getTraitType(){
        return traitType;
    }
    
    public int getDailyExp(){
        return dailyExp;
    }
    
    public void setDailyexp(int dailyExp){
        this.dailyExp = dailyExp;
    }
    
    public int getExpTotal(){
        return totalExp;
    }
    
    public int getTraitLevel(){
        return traitLevel;
    }
    
    public void setTraitLevel(int traitLevel){
        this.traitLevel = traitLevel;
    }
        
    public boolean calculateLevel(){
        if (totalExp < 0){
            totalExp = 0;
            localTotalExp = 0;
            return false;
        }        
        final int oldLevel = traitLevel;
        for (byte i = 0; i < 100; i++){
            if (ExpTable.getExpForTraitLevel(i) > localTotalExp){
                expirence = (short) (ExpTable.getExpForTraitLevel(i) - localTotalExp);
                traitLevel = (byte) (i - 1);
                return traitLevel > oldLevel;
            }
        }
        traitLevel = 100;
        totalExp = ExpTable.getExpForTraitLevel(traitLevel);
	localTotalExp = totalExp;
        return traitLevel > oldLevel;
    }    
    
    public void setExp(MapleTrait t, int exp, MapleCharacter c){        
        totalExp = exp;
        localTotalExp = exp;
        dailyExp = exp;
        
        if (dailyExp > t.dailyLimit){
            dailyExp = t.dailyLimit;
        } 
        
        if(traitLevel == 100){
            dailyExp = 0;
        }  
        
        if(traitLevel == 100){
            c.announce(MaplePacketCreator.showTraitMaxed(traitType));
        } else{
            c.announce(MaplePacketCreator.showTraitGain(traitType, totalExp));
        }
        
        calculateLevel();
    }

    private void increaseExp(int exp){
            totalExp += exp * ServerConstants.TRAIT_RATE;
            localTotalExp += exp * ServerConstants.TRAIT_RATE;
            dailyExp += exp * ServerConstants.TRAIT_RATE;
    }
    
    public void setGainExp(int exp, MapleCharacter c){
           if(!c.isGM()){
                if(dailyExp >= traitType.getDailyLimit() || traitLevel == 100){
                   c.announce(MaplePacketCreator.showTraitMaxed(traitType));
                   return;
                }  
           }
           increaseExp(exp * ServerConstants.TRAIT_RATE);
            
            if(dailyExp > traitType.dailyLimit){
               dailyExp = traitType.dailyLimit;
            }
            
            if(traitLevel == 100){
                dailyExp = 0;
            }            
            
            c.updateSingleStat(traitType.stat, localTotalExp, true);
            c.announce(MaplePacketCreator.showTraitGain(traitType, exp * ServerConstants.TRAIT_RATE));
            calculateLevel();
    }
    
    public void setUniversalGainExp(int exp, MapleCharacter c){
        if(!c.isGM()){
            if(dailyExp >= traitType.getDailyLimit() || traitLevel == 100){
               c.announce(MaplePacketCreator.showTraitMaxed(traitType));
               return;
            }  
        }
        
        increaseExp(exp * ServerConstants.TRAIT_RATE);         
        
        if(dailyExp > traitType.dailyLimit){
           dailyExp = traitType.dailyLimit;
        }   
        
       for(MapleTrait t: MapleTrait.values()){
        if(dailyExp < traitType.getDailyLimit()){
            if(traitLevel == 100){
                dailyExp = 0;
            }
            c.updateSingleStat(traitType.stat, localTotalExp, true);
            c.announce(MaplePacketCreator.showTraitGain(traitType, totalExp));   
            calculateLevel();
        }
    }
    }    
}
