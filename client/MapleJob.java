/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package client;

public enum MapleJob {
    BEGINNER(0),

    WARRIOR(100),
    FIGHTER(110), CRUSADER(111), HERO(112),
    PAGE(120), WHITEKNIGHT(121), PALADIN(122),
    SPEARMAN(130), DRAGONKNIGHT(131), DARKKNIGHT(132),

    MAGICIAN(200),
    FP_WIZARD(210), FP_MAGE(211), FP_ARCHMAGE(212),
    IL_WIZARD(220), IL_MAGE(221), IL_ARCHMAGE(222),
    CLERIC(230), PRIEST(231), BISHOP(232),

    BOWMAN(300),
    HUNTER(310), RANGER(311), BOWMASTER(312),
    CROSSBOWMAN(320), SNIPER(321), MARKSMAN(322),

    THIEF(400),
    ASSASSIN(410), HERMIT(411), NIGHTLORD(412),
    BANDIT(420), CHIEFBANDIT(421), SHADOWER(422),

    BLADE_RECRUIT(430), 
    BLADE_ACOLYTE(431), BLADE_SPECIALIST(432), BLADE_LORD(433), BLADE_MASTER(434), 
    
    PIRATE(500),
    BRAWLER(510), MARAUDER(511), BUCCANEER(512),
    GUNSLINGER(520), OUTLAW(521), CORSAIR(522),
    
    CANNON_BEGINNER(501), 
    CANNONEER(530), CANNON_BLASTER(531), CANNON_MASTER(532),
    
    JETT_BEGINNER(507),
    JETT1(508), JETT2(570), JETT3(571), JETT4(572), 
    
    MAPLELEAF_BRIGADIER(800),
    GM(900), SUPERGM(910),

    NOBLESSE(1000),
    DAWNWARRIOR1(1100), 
    DAWNWARRIOR2(1110), DAWNWARRIOR3(1111), DAWNWARRIOR4(1112),
    BLAZEWIZARD1(1200), 
    BLAZEWIZARD2(1210), BLAZEWIZARD3(1211), BLAZEWIZARD4(1212),
    WINDARCHER1(1300), 
    WINDARCHER2(1310), WINDARCHER3(1311), WINDARCHER4(1312),
    NIGHTWALKER1(1400), 
    NIGHTWALKER2(1410), NIGHTWALKER3(1411), NIGHTWALKER4(1412),
    THUNDERBREAKER1(1500), 
    THUNDERBREAKER2(1510), THUNDERBREAKER3(1511), THUNDERBREAKER4(1512),

    LEGEND(2000),
    ARAN1(2100),ARAN2(2110), ARAN3(2111), ARAN4(2112),
    
    EVAN_BEGINNER(2001), 
    EVAN1(2200), EVAN2(2210), EVAN3(2211), EVAN4(2212), 
    EVAN5(2213), EVAN6(2214), EVAN7(2215), EVAN8(2216), 
    EVAN9(2217), EVAN10(2218), 
    
    MERCEDES_BEGINNER(2002), 
    MERCEDES1(2300), MERCEDES2(2310), MERCEDES3(2311), MERCEDES4(2312), 
   
    PHANTOM_BEGINNER(2003), 
    PHANTOM1(2400), PHANTOM2(2410), PHANTOM3(2411), PHANTOM4(2412), 
   
    SHADE_BEGINNER(2005),
    SHADE1(2500), SHADE2(2510), SHADE3(2511), SHADE4(2512),  
   
    LUMINOUS_BEGINNER(2004),
    LUMINOUS1(2700), LUMINOUS2(2710), LUMINOUS3(2711), LUMINOUS4(2712),
    

    CITIZEN(3000), CITIZEN_DEMON_SLAYER(3001), 
    
    DEMON_SLAYER1(3100), 
    DEMON_SLAYER2(3110), DEMON_SLAYER3(3111), DEMON_SLAYER4(3112), 
    DEMON_AVENGER1(3101),
    DEMON_AVENGER2(3120), DEMON_AVENGER3(3121), DEMON_AVENGER4(3122),
    BATTLE_MAGE1(3200), 
    BATTLE_MAGE2(3210), BATTLE_MAGE3(3211), BATTLE_MAGE4(3212), 
    
    WILD_HUNTER1(3300), 
    WILD_HUNTER2(3310), WILD_HUNTER3(3311), WILD_HUNTER4(3312), 
   
    MECHANIC1(3500), 
    MECHANIC2(3510), MECHANIC3(3511), MECHANIC4(3512),
    
    XENON_BEGINNER(3002),
    XENON1(3600), XENON2(3610), XENON3(3611), XENON4(3612),
    
    HAYATO_BEGINNER(4001),
    HAYATO1(4100), HAYATO2(4110), HAYATO3(4111), HAYATO4(4112),
    
    KANNA_BEGINNER(4002),
    KANNA1(4200), KANNA2(4210), KANNA3(4211), KANNA4(4212),
    
    
    MIHILE(5000),
    MIHILE1(5100), MIHILE2(5110), MIHILE3(5111),MIHILE4(5112),
    
    KAISER_BEGINNER(6000),
    KAISER1(6100), KAISER2(6110), KAISER3(6111), KAISER4(6112),
    
    ANGELIC_BEGINNER(6001),
    ANGELIC1(6500), ANGELIC2(6510), ANGELIC3(6511), ANGELIC4(6512),
    
    
    ZERO_BEGINNER(10000),
    ZERO1(10100), ZERO2(10110), ZERO3(10111), ZERO4(10112),
    
    BEAST_BEGINNER(11200),
    BEAST1(11210), BEAST2(11211), BEAST3(11212),
    
    ADDITIONAL_SKILLS(9000);

    final int jobid;

    private MapleJob(int id) {
        jobid = id;
    }

    public int getId() {
        return jobid;
    }

    public static MapleJob getById(int id) {
        for (MapleJob l : MapleJob.values()) {
            if (l.getId() == id) {
                return l;
            }
        }
        return null;
    }

    public static MapleJob getBy5ByteEncoding(int encoded) {
        switch (encoded) {
            case 2:
                return WARRIOR;
            case 4:
                return MAGICIAN;
            case 8:
                return BOWMAN;
            case 16:
                return THIEF;
            case 32:
                return PIRATE;
            case 1024:
                return NOBLESSE;
            case 2048:
                return DAWNWARRIOR1;
            case 4096:
                return BLAZEWIZARD1;
            case 8192:
                return WINDARCHER1;
            case 16384:
                return NIGHTWALKER1;
            case 32768:
                return THUNDERBREAKER1;
            default:
                return BEGINNER;
        }
    }

    public boolean isA(MapleJob basejob) {        
        return getId() >= basejob.getId() && getId() / 100 == basejob.getId() / 100;
    }
}
