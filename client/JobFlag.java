/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

/**
 *
 * @author Steven
 */
public enum JobFlag {

    DISABLED(0),
    ENABLED(1);

    private int flag;

    private JobFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }
}
