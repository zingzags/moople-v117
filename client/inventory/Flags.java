/*
This file is part of the OdinMS Maple Story Server
Copyright (C) 2008 ~ 2010 Patrick Huy <patrick.huy@frz.cc> 
Matthias Butz <matze@odinms.de>
Jan Christian Meyer <vimes@odinms.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation. You may not use, modify
or distribute this program under any other version of the
GNU Affero General Public License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package client.inventory;
/*
 * @Author: Lithium Developers
 * Note: The only real reason why I added this is because of organization.
 */
public enum Flags {
    LOCK(0x01),
    SPIKES(0x02),
    COLD(0x04),
    UNTRADEABLE(0x08),
    KARMA(0x10), // send this to cs items and become tradable?
    KARMA_USE(0x02),
    CHARM_EQUIPPED(0x20),
    ANDROID_ACTIVATED(0x40),
    CRAFTED(0x80), 
    CRAFTED_USE(0x10),
    SHIELD_WARD(0x100),
    LUCKY_DAY_SCROLL(0x200),
    KARMA_USE2(0x400),
    KARMA_2(0x1000),
    SAFETY_SCROLL(0x2000),
    GUARDIAN_SCROLL(0x4000);
    
    private short i;

    private Flags(int i){
       this.i = (short) i;
    }

    public short getValue(){
        return i;
    }

    public boolean check(Equip item){
        return (item.getFlag() & i) == i;
    }    
}
