/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package client.inventory;

public enum MapleWeaponType {
    NOT_A_WEAPON(0),
    AXE1H(1.2),
    AXE2H(1.32),
    BLUNT1H(1.2),
    BLUNT2H(1.32),
    BOW(1.3),
    CANE(1.3),
    CANNON(1.8),
    CARTE(2.0),    
    CLAW(1.75),
    CROSSBOW(1.35),
    DAGGER(1.3),
    DUAL_BOW(1.3), //beyond op    
    GUN(1.5),
    KATARA(1.3),
    KNUCKLE(1.7),
    MAGIC_ARROW(2.0),    
    POLE_ARM(1.49),
    SPEAR(1.49),
    STAFF(1.0),
    SWORD1H(1.2),
    SWORD2H(1.32),
    WAND(1.0);


    
    private double damageMultiplier;

    private MapleWeaponType(double maxDamageMultiplier) {
        this.damageMultiplier = maxDamageMultiplier;
    }

    public double getMaxDamageMultiplier() {
        return damageMultiplier;
    }
}
