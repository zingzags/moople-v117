/*
 This file is part of the OdinMS Maple Story Server
 Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
 Matthias Butz <matze@odinms.de>
 Jan Christian Meyer <vimes@odinms.de>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation version 3 as published by
 the Free Software Foundation. You may not use, modify or distribute
 this program under any other version of the GNU Affero General Public
 License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package client.inventory;

import client.MapleClient;
import constants.ItemConstants;
import java.util.LinkedList;
import java.util.List;
import server.MapleItemInformationProvider;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.Randomizer;

public class Equip extends Item {

    public static enum ScrollResult {

        FAIL(0), SUCCESS(1), CURSE(2);
        private int value = -1;

        private ScrollResult(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    private byte upgradeSlots, enhance;
    private byte level, itemLevel;
    private short flag;
    private short str, dex, _int, luk, hp, mp, watk, matk, wdef, mdef, acc, avoid, hands, speed, jump, vicious, charm, pvpDMG, HpR, MpR;
    private int durability = -1, incSkill, potential1 = 0, potential2 = 0, potential3 = 0, potential4, potential5, socket1, socket2, socket3;
    private float itemExp;
    private int ringid = -1;
    private boolean wear = false;

    public Equip(int id, short position) {
        super(id, position, (short) 1);
        this.itemExp = 0;
        this.itemLevel = 1;
    }

    public Equip(int id, short position, int slots) {
        super(id, position, (short) 1);
        this.upgradeSlots = (byte) slots;
        this.itemExp = 0;
        this.itemLevel = 1;
    }

    @Override
    public Item copy() {
        Equip ret = new Equip(getItemId(), getPosition(), getUpgradeSlots());
        ret.str = str;
        ret.dex = dex;
        ret._int = _int;
        ret.luk = luk;
        ret.hp = hp;
        ret.mp = mp;
        ret.matk = matk;
        ret.mdef = mdef;
        ret.watk = watk;
        ret.wdef = wdef;
        ret.acc = acc;
        ret.avoid = avoid;
        ret.hands = hands;
        ret.speed = speed;
        ret.jump = jump;
        ret.flag = flag;
        ret.vicious = vicious;
        ret.upgradeSlots = upgradeSlots;
        ret.itemLevel = itemLevel;
        ret.itemExp = itemExp;
        ret.level = level;
        ret.durability = durability;
        ret.enhance = enhance;
        ret.potential1 = potential1;
        ret.potential2 = potential2;
        ret.potential3 = potential3;
        ret.potential4 = potential4;
        ret.potential5 = potential5;        
        ret.socket1 = socket1;
        ret.socket2 = socket2;
        ret.socket3 = socket3;
        ret.charm = charm;
        ret.pvpDMG = pvpDMG;
        ret.incSkill = incSkill;   
        ret.HpR = HpR;
        ret.MpR = MpR;
        ret.log = new LinkedList<>(log);
        ret.setOwner(getOwner());
        ret.setQuantity(getQuantity());
        ret.setExpiration(getExpiration());
        ret.setGiftFrom(getGiftFrom());
        return ret;
    }

    @Override
    public short getFlag() {
        return flag;
    }

    @Override
    public byte getType() {
        return 1;
    }

    public byte getUpgradeSlots() {
        return upgradeSlots;
    }

    public short getStr() {
        return str;
    }

    public short getDex() {
        return dex;
    }

    public short getInt() {
        return _int;
    }

    public short getLuk() {
        return luk;
    }

    public short getHp() {
        return hp;
    }

    public short getMp() {
        return mp;
    }
    
    public short getMpR(){
        return MpR;
    }
    
    public short getHpR(){
        return HpR;
    }
    
    public short getWatk() {
        return watk;
    }

    public short getMatk() {
        return matk;
    }

    public short getWdef() {
        return wdef;
    }

    public short getMdef() {
        return mdef;
    }

    public short getAcc() {
        return acc;
    }

    public short getAvoid() {
        return avoid;
    }

    public short getHands() {
        return hands;
    }

    public short getSpeed() {
        return speed;
    }

    public short getJump() {
        return jump;
    }

    public short getVicious() {
        return vicious;
    }

    @Override
    public void setFlag(short flag) {
        this.flag = flag;
    }
    
    public void removeFlag(short flag) {
        this.flag &= ~flag;
    }
    
    public void setStr(short str) {
        this.str = str;
    }

    public void setDex(short dex) {
        this.dex = dex;
    }

    public void setInt(short _int) {
        this._int = _int;
    }

    public void setLuk(short luk) {
        this.luk = luk;
    }

    public void setHp(short hp) {
        this.hp = hp;
    }

    public void setMp(short mp) {
        this.mp = mp;
    }

    public void setWatk(short watk) {
        this.watk = watk;
    }

    public void setMatk(short matk) {
        this.matk = matk;
    }

    public void setWdef(short wdef) {
        this.wdef = wdef;
    }

    public void setMdef(short mdef) {
        this.mdef = mdef;
    }

    public void setAcc(short acc) {
        this.acc = acc;
    }

    public void setAvoid(short avoid) {
        this.avoid = avoid;
    }

    public void setHands(short hands) {
        this.hands = hands;
    }

    public void setSpeed(short speed) {
        this.speed = speed;
    }

    public void setJump(short jump) {
        this.jump = jump;
    }

    public void setVicious(short vicious) {
        this.vicious = vicious;
    }

    public void setMpR(short MpR){
        this.MpR = MpR;
    } 
    
    public void setHpR(short HpR){
        this.HpR = HpR;
    }    
    
    public void setUpgradeSlots(byte upgradeSlots) {
        this.upgradeSlots = upgradeSlots;
    }

    public byte getLevel() {
        return level;
    }

    public void setLevel(byte level) {
        this.level = level;
    }

    public int getDurability(){
        return durability;
    }

    public void setDurability(int dur){
        this.durability = dur;
    }

    public byte getEnhance(){
        return enhance;
    }

    public void setEnhance(byte en){
        this.enhance = en;
    }
    
    public byte getPotential() {
        int pots = potential1 + potential2 + potential3;
        if (potential1 > 40000 || potential2 > 40000 || potential3 > 40000) {
            return ItemConstants.LEGENDARY; // legendary
        } else if (potential1 > 30000 || potential2 > 30000 || potential3 > 30000) {
            return ItemConstants.UNIQUE; // unique
        } else if (potential1 > 20000 || potential2 > 20000 || potential3 > 20000) {
            return ItemConstants.EPIC; // epic
        } else if (potential1 > 10000 || potential2 > 10000 || potential3 > 10000) {
            return ItemConstants.RARE; // rare
        } else if (pots < 0) {
            return 1; // hidden
        }
        return 0;
    }
    
    public int getPotential1(){
        return potential1;
    }

    public void setPotential1(int en){
        this.potential1 = en;
    }

    public int getPotential2(){
        return potential2;
    }

    public void setPotential2(int en){
        this.potential2 = en;
    }

    public int getPotential3(){
        return potential3;
    }

    public void setPotential3(int en){
        this.potential3 = en;
    }
     
    public int getPotential4(){
        return potential4;
    }

    public void setPotential4(int en){
        this.potential4 = en;
    }
    
    public int getPotential5(){
        return potential5;
    }

    public void setPotential5(int en){
        this.potential5 = en;
    }
    
    public int getIncSkill(){
        return incSkill;
    }

    public void setIncSkill(int inc){
        this.incSkill = inc;
    }

    public short getCharm(){
        return charm;
    }

    public short getPVPDMG(){
        return pvpDMG;
    }

    public void setCharm(short s){
        this.charm = s;
    }

    public void setPVPDMG(short p){
        this.pvpDMG = p;
    }
 
    
    public int getSocket1(){
        return socket1;
    }

    public void setSocket1(final int socket){
         this.socket1 = socket;
    }
    
    public int getSocket2(){
        return socket2;
    }

    public void setSocket2(int socket2){
        this.socket2 = socket2;
    }

    public int getSocket3(){
        return socket3;
    }

    public void setSocket3(int socket3){
        this.socket3 = socket3;
    }
    
    public void resetPotential(boolean epic) {
        int rank = epic ? -18 : Randomizer.nextInt(100) < 4 ? (Randomizer.nextInt(100) < 4 ? (Randomizer.nextInt(100) < 4 ? -20 : -19) : -18) : -17;
        setPotential1((short) rank);
        setPotential2((short) (epic && getPotential3() > 0 ? rank : (Randomizer.nextInt(10) == 0 ? rank : 0))); //1/10 chance of 3 line
        setPotential3((short) 0); //just set it theoretically
    }

    public void renewPotential(boolean prem, boolean superr) {
        final int rank = (getPotential() != (superr ? 20 : 19) && Randomizer.nextInt(100) < (superr ? (16 - (getPotential() - 15) * 2) : 6)) ? -(getPotential() + 1) : -(getPotential());
        setPotential1((short) rank);
        setPotential2((short) ((getPotential3() > 0 || (prem && Randomizer.nextInt(10) == 0)) ? rank : 0)); //1/10 chance of 3 line
        setPotential3((short) 0); //just set it theoretically
    }
    
    public void gainLevel(MapleClient c, boolean timeless) {
        List<Pair<String, Integer>> stats = MapleItemInformationProvider.getInstance().getItemLevelupStats(getItemId(), itemLevel, timeless);
        for (Pair<String, Integer> stat : stats) {
            switch (stat.getLeft()) {
                case "incDEX":
                    dex += stat.getRight();
                    break;
                case "incSTR":
                    str += stat.getRight();
                    break;
                case "incINT":
                    _int += stat.getRight();
                    break;
                case "incLUK":
                    luk += stat.getRight();
                    break;
                case "incMHP":
                    hp += stat.getRight();
                    break;
                case "incMMP":
                    mp += stat.getRight();
                    break;
                case "incPAD":
                    watk += stat.getRight();
                    break;
                case "incMAD":
                    matk += stat.getRight();
                    break;
                case "incPDD":
                    wdef += stat.getRight();
                    break;
                case "incMDD":
                    mdef += stat.getRight();
                    break;
                case "incEVA":
                    avoid += stat.getRight();
                    break;
                case "incACC":
                    acc += stat.getRight();
                    break;
                case "incSpeed":
                    speed += stat.getRight();
                    break;
                case "incJump":
                    jump += stat.getRight();
                    break;
            }
        }
        this.itemLevel++;
        c.announce(MaplePacketCreator.showEquipmentLevelUp());
        c.getPlayer().getMap().broadcastMessage(c.getPlayer(), MaplePacketCreator.showForeignEffect(c.getPlayer().getId(), 15));
        c.getPlayer().forceUpdateItem(this);
    }

    public int getItemExp() {
        return (int) itemExp;
    }

    public void gainItemExp(MapleClient c, int gain, boolean timeless) {
        int expneeded = timeless ? (10 * itemLevel + 70) : (5 * itemLevel + 65);
        float modifier = 364 / expneeded;
        float exp = (expneeded / (1000000 * modifier * modifier)) * gain;
        itemExp += exp;
        if (itemExp >= 364) {
            itemExp = (itemExp - 364);
            gainLevel(c, timeless);
        } else {
            c.getPlayer().forceUpdateItem(this);
        }
    }

    public void setItemExp(int exp) {
        this.itemExp = exp;
    }

    public void setItemLevel(byte level) {
        this.itemLevel = level;
    }

    @Override
    public void setQuantity(short quantity) {
        if (quantity < 0 || quantity > 1) {
            throw new RuntimeException("Setting the quantity to " + quantity + " on an equip (itemid: " + getItemId() + ")");
        }
        super.setQuantity(quantity);
    }

    public void setUpgradeSlots(int i) {
        this.upgradeSlots = (byte) i;
    }

    public void setVicious(int i) {
        this.vicious = (short) i;
    }

    public int getRingId() {
        return ringid;
    }

    public void setRingId(int id) {
        this.ringid = id;
    }

    public boolean isWearing() {
        return wear;
    }

    public void wear(boolean yes) {
        wear = yes;
    }

    public byte getItemLevel() {
        return itemLevel;
    }
}