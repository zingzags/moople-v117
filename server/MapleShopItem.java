/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package server;

/**
 *
 * @author Matze
 */
public class MapleShopItem {
    private short buyable;
    private int itemId;
    private int price;
    private int coinItem;
    private byte discount;
    private int questItem;
    private short quantity;
    private int category;
    private int timeLimit;
    private int minLevel;
    private byte rank;
    
    public MapleShopItem(int itemId, int price, short buyable) {
	this.buyable = buyable;
	this.itemId = itemId;
	this.price = price;
	this.coinItem = 0;
	this.questItem = 0;
	this.rank = (byte)0;
        this.discount = (byte) 0;
        this.timeLimit = 0;
        this.minLevel = 0;
        this.category = 0;
        this.quantity = (short) 0;
    }    
    
    
    public MapleShopItem(short buyable, int itemId, int price, int coinItem) {
        this.buyable = buyable;
        this.itemId = itemId;
        this.price = price;
        this.coinItem = coinItem;
        this.discount = (byte) 0;
        this.questItem = 0;
        this.quantity = 0;
        this.category = 0;
        this.timeLimit = 0;
        this.minLevel = 0;
        this.rank = 0;
    }

    public MapleShopItem(short buyable, int itemId, int price, int coinItem, byte discount, int questItem, short quantity, int category, int timeLimit, int minLevel, byte rank) {
        this.buyable = buyable;
        this.itemId = itemId;
        this.price = price;
        this.coinItem = coinItem;
        this.discount = (byte) discount;
        this.questItem = questItem;
        this.quantity = (short) quantity;
        this.category = category;
        this.timeLimit = timeLimit;
        this.minLevel = minLevel;
        this.rank = (byte) rank;
    }    
    
    public short getBuyable() {
        return buyable;
    }

    public int getItemId() {
        return itemId;
    }

    public int getPrice() {
        return price;
    }

    public int getCoinItem() {
        return coinItem;
    }
    
    public byte getDiscount(){
        return discount;
    }
    
    public int getQuestItem(){
        return questItem;
    }
    
    public short getQuantity(){
        return quantity;
    }
    
    public int getCategory(){
        return category;
    }
    
    public int getTimeLimit(){
        return timeLimit;
    }
    
    public int getMinLevel(){
        return minLevel;
    }
    
    public byte getRank(){
        return rank;
    }
}
