/* 
 This file is part of the OdinMS Maple Story Server 
 Copyright (C) 2008 ~ 2010 Patrick Huy <patrick.huy@frz.cc>  
 Matthias Butz <matze@odinms.de> 
 Jan Christian Meyer <vimes@odinms.de> 

 This program is free software: you can redistribute it and/or modify 
 it under the terms of the GNU Affero General Public License version 3 
 as published by the Free Software Foundation. You may not use, modify 
 or distribute this program under any other version of the 
 GNU Affero General Public License. 

 This program is distributed in the hope that it will be useful, 
 but WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 GNU Affero General Public License for more details. 

 You should have received a copy of the GNU Affero General Public License 
 along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */ 
package server.quest;

import java.io.File; 
import java.util.ArrayList; 
import java.util.List; 

import provider.MapleData; 
import provider.MapleDataProvider; 
import provider.MapleDataProviderFactory; 
import provider.MapleDataTool; 

/** 
 * Populates a list of NXQuests read 
 * from WZ XMLS to be used in  
 * addNXQuestInfo 
 * 
 * @author SharpAceX (Alan) 
 *  
 */ 

public class NXQuestFactory { 

    public static final List<NXQuest> NXQuests; 
     
    static { 
        NXQuests = new ArrayList<NXQuest>(); 
        File file = new File(System.getProperty("wzpath") + "/Etc.wz"); 
        MapleDataProvider etcwz= MapleDataProviderFactory.getDataProvider(file); 
        MapleData cashAccountLimit = etcwz.getData("CashAccountLimit.img"); 
        for (MapleData data : cashAccountLimit.getChildren()){ 
            String name = data.getName(); 
            String enable = MapleDataTool.getIntConvert("enable", data, 1) + ""; 
            String limit = MapleDataTool.getIntConvert("limitCount", data, 1) + ""; 
            NXQuests.add(new NXQuest(name, enable, limit)); 
        } 
    } 
     
    public static class NXQuest { 
         
        private String name; 
        private String enable; 
        private String limit; 
         
        /** 
         * Creates a new instance of NXQuest 
         * 
         * @param name 
         * @param enable 
         * @param limit 
         */ 
         
        public NXQuest(String name, String enable, String limit){ 
            this.name = name; 
            this.enable = enable; 
            this.limit = limit; 
        } 

        public String getName() { 
            return name; 
        } 

        public String getEnable() { 
            return enable; 
        } 

        public String getLimit() { 
            return limit; 
        } 
    } 
}  