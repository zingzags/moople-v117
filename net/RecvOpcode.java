/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package net;

public enum RecvOpcode {
    CUSTOM_PACKET(0x3713),//13 37 lol
    CLIENT_HELLO(0x40),
    LOGIN_PASSWORD(0x41),
    GUEST_LOGIN(0x1FF),
    CHARLIST_REQUEST(0x44),
    SERVERSTATUS_REQUEST(0x1E),
    ACCEPT_TOS(0x1F),
    SET_GENDER(0x1FF),
    AFTER_LOGIN(0x1FFF),   // Skip over 
    REGISTER_PIN(0x1FFF),  
    SERVERLIST_REQUEST(0x23),
    SERVERLIST_REREQUEST(0x1A),
    PLAYER_DC(0x1EFFF),
    
    VIEW_ALL_CHAR(0x1FF),
    PICK_ALL_CHAR(0x1FF),
    
    PLAYER_LOGGEDIN(0x28),
    CHECK_CHAR_NAME(0x29),
    CREATE_CHAR(0x47),
    DELETE_CHAR(0x2D),
    PONG(0x48),
    CRASH_INFO(0x31),
    CLIENT_START(0x3A),
    CLIENT_ERROR(0x4B),
    
    STRANGE_DATA(0x1FF),
    RELOG(0x1FF),
    CLIENT_AUTH(0x33),
    CHAR_SELECT(0x46),  // Skip Over  
    REGISTER_PIC(0x34),
    CHAR_SELECT_WITH_PIC(0x49),
    CHANGE_PIC(0x50),
    VIEW_ALL_PIC_REGISTER(0x1FF),
    VIEW_ALL_WITH_PIC(0x1FF),
    CHANGE_MAP(0x1FF),
    CHANGE_CHANNEL(0x54),
    ENTER_CASHSHOP(0x1FF),
    MOVE_PLAYER(0x62),
    CANCEL_CHAIR(0x1FF),
    USE_CHAIR(0x1FF),
    CLOSE_RANGE_ATTACK(0x4F),
    RANGED_ATTACK(0x2D),
    MAGIC_ATTACK(0x2E),
    TOUCH_MONSTER_ATTACK(0x2F),
    TAKE_DAMAGE(0x54),
    GENERAL_CHAT(0x6F),
    CLOSE_CHALKBOARD(0x57),
    FACE_EXPRESSION(0x1FF),
    USE_ITEMEFFECT(0x1FFF),
    USE_DEATHITEM(0x1FFFF),
    MONSTER_BOOK_COVER(0x1FF),
    NPC_TALK(0x68),
    REMOTE_STORE(0x69),
    NPC_TALK_MORE(0x6A),
    NPC_SHOP(0x6B),
    STORAGE(0x84),
    HIRED_MERCHANT_REQUEST(0x6D),
    FREDRICK_ACTION(0x6F),
    DUEY_ACTION(0x6F),
    ADMIN_SHOP(0x74),//oh lol
    ITEM_SORT(0x75),
    ITEM_SORT2(0x76),
    ITEM_MOVE(0x99),
    MOVE_BAG(0x78),
    SWITCH_BAG(0x79),
    USE_ITEM(0x7B),
    CANCEL_ITEM_EFFECT(0x1FF),
    USE_SUMMON_BAG(0x7E),
    PET_FOOD(0x4C),
    USE_MOUNT_FOOD(0x80),
    SCRIPTED_ITEM(0x4E),
    USE_CASH_ITEM(0x4F),

    USE_CATCH_ITEM(0x51),
    USE_SKILL_BOOK(0x52),
    USE_TELEPORT_ROCK(0x54),
    USE_RETURN_SCROLL(0x55),
    USE_UPGRADE_SCROLL(0x94),
    USE_FLAG_SCROLL(0x95),
    USE_EQUIP_SCROLL(0x96),
    USE_POTENTIAL_SCROLL(0x98),
    USE_MAGNIFY_GLASS (0x9C), 
    DISTRIBUTE_AP(0x9D),
    AUTO_DISTRIBUTE_AP(0x9E),
    HEAL_OVER_TIME(0xDF),
    DISTRIBUTE_SP(0xA2),
    SPECIAL_MOVE(0xA3),
    USE_TITLE(0x75),
    CANCEL_BUFF(0xA4),
    SKILL_EFFECT(0xA5),
    MESO_DROP(0xA6),
    GIVE_FAME(0xA7),
    CHAR_INFO_REQUEST(0xA9),
    SPAWN_PET(0xAA),
    CANCEL_DEBUFF(0xB1),
    CHANGE_MAP_SPECIAL(0xB2),
    USE_INNER_PORTAL(0xB3),
    TROCK_ADD_MAP(0xB7),
    REPORT(0xB8), //B8-B9 IDK
    QUEST_ACTION(0xBA),
    //lolno
    SKILL_MACRO(0xBF),
    SPOUSE_CHAT(0xE0),
    USE_ITEM_REWARD(0xE1),
    MAKER_SKILL(0xE2),
    USE_REMOTE(0xE3),
    ADMIN_CHAT(0xE3),
    PARTYCHAT(0xE4),
    WHISPER(0xE5),
    MESSENGER(0xE7),
    PLAYER_INTERACTION(0xE8),
    PARTY_OPERATION(0xE9),
    DENY_PARTY_REQUEST(0xEA),
    GUILD_OPERATION(0xEE),
    DENY_GUILD_REQUEST(0xEF),
    ADMIN_COMMAND(0xF0),
    ADMIN_LOG(0xF1),
    BUDDYLIST_MODIFY(0x142),
    NOTE_ACTION(0xF3),
    USE_DOOR(0xF5),
    USE_MECH_DOOR(0xF6),
    CHANGE_KEYMAP(0x154),
    RPS_ACTION(0xF9),
    RING_ACTION(0xFA),
    WEDDING_ACTION(0xFB),
    OPEN_FAMILY(0x102),
    ADD_FAMILY(0x103),
    ACCEPT_FAMILY(0x106),
    USE_FAMILY(0x107),
    ALLIANCE_OPERATION(0xFF),
    BBS_OPERATION(0x10B),
    OPEN_EXTENDED_BAG(0x9B),
    ENTER_MTS(0x9C),
    USE_SOLOMON_ITEM(0xFFF),
    USE_GACHA_EXP(0xCD),
    CLICK_GUIDE(0x133),
    ARAN_COMBO_COUNTER(0x134),
    MOVE_PET(0x135),
    PET_CHAT(0x136),
    PET_COMMAND(0x137),
    PET_LOOT(0x138),
    PET_AUTO_POT(0x139),
    PET_EXCLUDE_ITEMS(0x140),
    MOVE_SUMMON(0x13D),
    SUMMON_ATTACK(0x13E),
    DAMAGE_SUMMON(0x13F),
    BEHOLDER(0x140),
    MOVE_LIFE(0x161),
    AUTO_AGGRO(0xBD),
    MOB_DAMAGE_MOB_FRIENDLY(0xC0),
    MONSTER_BOMB(0xC1),
    MOB_DAMAGE_MOB(0xC2),
    NPC_ACTION(0x28C),
    ITEM_PICKUP(0xCA),
    DAMAGE_REACTOR(0xCD),
    TOUCHING_REACTOR(0xCE),
    TEMP_SKILL(0xCF),
    MAPLETV(0xFFFE),//Don't know
    SNOWBALL(0xD3),
    LEFT_KNOCKBACK(0xD4),
    COCONUT(0x186),
    MATCH_TABLE(0xD6),//Would be cool if I ever get it to work :)
    MONSTER_CARNIVAL(0xDA),
    PARTY_SEARCH_REGISTER(0xDC),
    PARTY_SEARCH_START(0xDE),
    PLAYER_UPDATE(0xDF),
    CHECK_CASH(0xE4),
    CASHSHOP_OPERATION(0xE5),
    COUPON_CODE(0x1A7),
    OPEN_ITEMUI(0x99999),
    CLOSE_ITEMUI(0xEC),
    USE_ITEMUI(0xED),
    MTS_OPERATION(0xFD),
    USE_MAPLELIFE(0xFE),
    USE_HAMMER(0x104),
    BUTTON_PRESSED(0x207),
    MOVE_DRAGON(0x145),
    PINKBEAN_CHOCO_OPEN(0x130),
    PINKBEAN_CHOCO_SUMMON(0x131);
    private int code = -2;

    private RecvOpcode(int code) {
        this.code = code;
    }

    public int getValue() {
        return code;
    }
}