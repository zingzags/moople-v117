/*
 This file is part of the OdinMS Maple Story Server
 Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
 Matthias Butz <matze@odinms.de>
 Jan Christian Meyer <vimes@odinms.de>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation version 3 as published by
 the Free Software Foundation. You may not use, modify or distribute
 this program under any other version of the GNU Affero General Public
 License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.server.channel.handlers;

import client.MapleClient;
import client.MapleTraits;
import client.Skill;
import client.SkillFactory;
import client.inventory.Equip;
import client.inventory.Equip.ScrollResult;
import client.inventory.Flags;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import client.inventory.ModifyInventory;
import constants.ItemConstants;
import java.util.ArrayList;
import java.util.List;
import net.AbstractMaplePacketHandler;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 * @author Matze
 * @author Frz
 */
public final class ScrollHandler extends AbstractMaplePacketHandler {

    @Override
    public final void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        slea.readInt(); // whatever...
        scroll(c, (byte) slea.readShort(), slea.readShort(), slea.readShort(), 0);
    }
    
    public static void scroll(final MapleClient c, short slot, short dst, short ws, int vegas){
        boolean whiteScroll = false; // white scroll being used?
        boolean legendarySpirit = false; // legendary spirit skill
        if ((ws & 2) == 2) {
            whiteScroll = true;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        Equip toScroll = (Equip) c.getPlayer().getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
        Skill LegendarySpirit = SkillFactory.getSkill(1003);
        
        if (c.getPlayer().getSkillLevel(LegendarySpirit) > 0 && dst >= 0) {
            legendarySpirit = true;
            toScroll = (Equip) c.getPlayer().getInventory(MapleInventoryType.EQUIP).getItem(dst);
        }
        
        byte oldLevel = toScroll.getLevel();
        byte oldState = toScroll.getPotential();
        short oldFlag = toScroll.getFlag();
        byte oldEnhance = toScroll.getEnhance();
        int scrollSucessIncrease = 0;
        MapleInventory useInventory = c.getPlayer().getInventory(MapleInventoryType.USE);
        Item scroll = useInventory.getItem(slot);
        Item wscroll = null;
        List<Integer> scrollReqs = ii.getScrollReqs(scroll.getItemId());
    
        if(isSpecialScroll(scroll.getItemId()) || isEnhancement(scroll.getItemId()) || isPotentialScroll(scroll.getItemId())){          
            whiteScroll = false;
        }
        
        if (isPotentialScroll(scroll.getItemId())) {
            boolean isEpic = scroll.getItemId() / 100 == 20497;
            if (!isEpic && toScroll.getPotential() >= 1 || isEpic && toScroll.getPotential() >= ItemConstants.EPIC || toScroll.getLevel() == 0 && toScroll.getUpgradeSlots() == 0 && toScroll.getItemId() / 10000 != 135 && !isEpic || vegas > 0 || ii.isCash(toScroll.getItemId())) {
             c.announce(MaplePacketCreator.getInventoryFull());
             return;
            }
        } else {   
            if(!isSpecialScroll(scroll.getItemId())) { 
                if(!isEnhancement(scroll.getItemId())) {
                    if (toScroll.getUpgradeSlots() < 1) {
                        c.announce(MaplePacketCreator.getInventoryFull());
                        return;
                    }     
                    if (scrollReqs.size() > 0 && !scrollReqs.contains(toScroll.getItemId())) {
                        c.announce(MaplePacketCreator.getInventoryFull());
                        return;
                    }
                    if (whiteScroll) {
                        wscroll = useInventory.findById(2340000);
                        if (wscroll == null || wscroll.getItemId() != 2340000) {
                            whiteScroll = false;
                        }
                    }
                    if (scroll.getItemId() != 2049100){// && !isSpecialScroll(scroll.getItemId()) && !isEnhancement(scroll.getItemId())) {
                        if (!canScroll(scroll.getItemId(), toScroll.getItemId())) {
                            return;
                        }
                    }
                }
            }
        }
        
        if (scroll.getQuantity() < 1) {
            return;
        }
               
        scrollSucessIncrease += c.getPlayer().getTrait(MapleTraits.MapleTrait.DILLIGENCE).getTraitLevel()/10;
        Equip scrolled = (Equip) ii.scrollEquipWithId(toScroll, scroll.getItemId(), whiteScroll, c.getPlayer().isGM(), scrollSucessIncrease);
        ScrollResult scrollSuccess = Equip.ScrollResult.FAIL; // fail
        
        if (scrolled == null) {
            if(Flags.SHIELD_WARD.check(toScroll)){
               scrolled = toScroll;
               scrolled.removeFlag(Flags.SHIELD_WARD.getValue());
            } else {
               scrollSuccess = Equip.ScrollResult.CURSE;
            }
        } else if (scrolled.getLevel() > oldLevel || (isCleanSlate(scroll.getItemId()) && scrolled.getLevel() == oldLevel + 1) || scrolled.getPotential() > oldState || scrolled.getEnhance() > oldEnhance) {
            scrollSuccess = Equip.ScrollResult.SUCCESS;
            if(Flags.SHIELD_WARD.check(scrolled) && (isCleanSlate(scroll.getItemId()) || isPotentialScroll(scroll.getItemId()) || isEnhancement(scroll.getItemId()) || isGoodness(scroll.getItemId()) || isMiraculous(scroll.getItemId()))){
               scrolled.removeFlag(Flags.SHIELD_WARD.getValue());
            }
        } else if(scrolled.getFlag() > oldFlag){
            scrollSuccess = Equip.ScrollResult.SUCCESS;  
        }
        
        if(!Flags.GUARDIAN_SCROLL.check(scrolled) || Flags.GUARDIAN_SCROLL.check(scrolled) && scrollSuccess == Equip.ScrollResult.SUCCESS){ 
            useInventory.removeItem(scroll.getPosition(), (short) 1, false);
        }
        
        if(Flags.GUARDIAN_SCROLL.check(scrolled)){
            scrolled.removeFlag(Flags.GUARDIAN_SCROLL.getValue());
        }
        
        if (whiteScroll) {
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, wscroll.getPosition(), (short) 1, false, false);
        }
        
        if(Flags.LUCKY_DAY_SCROLL.check(scrolled) && canScroll(scroll.getItemId(), scrolled.getItemId())){
            scrolled.removeFlag(Flags.LUCKY_DAY_SCROLL.getValue());
        }
        
        if(Flags.SAFETY_SCROLL.check(scrolled) && canScroll(scroll.getItemId(), scrolled.getItemId())){
            scrolled.removeFlag(Flags.SAFETY_SCROLL.getValue());
        }
        
        final List<ModifyInventory> mods = new ArrayList<>();
        mods.add(new ModifyInventory(scroll.getQuantity() > 0 ? 1 : 3, scroll));
        if (scrollSuccess == Equip.ScrollResult.CURSE) {
            mods.add(new ModifyInventory(3, toScroll));
            if (dst < 0) {
                c.getPlayer().getInventory(MapleInventoryType.EQUIPPED).removeItem(toScroll.getPosition());
            } else {
                c.getPlayer().getInventory(MapleInventoryType.EQUIP).removeItem(toScroll.getPosition());
            }
        } else {
            mods.add(new ModifyInventory(3, scrolled));
            mods.add(new ModifyInventory(0, scrolled));
        }
        c.announce(MaplePacketCreator.modifyInventory(true, mods));
        c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.getScrollEffect(c.getPlayer().getId(), scrollSuccess, legendarySpirit, scroll.getItemId(), scrolled.getItemId()));
        if (dst < 0 && (scrollSuccess == Equip.ScrollResult.SUCCESS || scrollSuccess == Equip.ScrollResult.CURSE)) {
            c.getPlayer().equipChanged();
        }
    }
    
    private static boolean isCleanSlate(int scrollId) {
        return scrollId > 2048999 && scrollId < 2049004;
    }

    public static boolean canScroll(int scrollid, int itemid) {
        return (scrollid / 100) % 100 == (itemid / 10000) % 100;
    }
    
    private static boolean isPotentialScroll(int scrollId){
        return scrollId >= 2049400 && scrollId <= 2049408 && scrollId >= 2049413 && scrollId <= 2049703;
    }
    
    private static boolean isInnocence(int scrollId){
        return scrollId == 2049600 || scrollId == 2049601 || scrollId == 2049604;
    }
    
    private static boolean isEnhancement(int scrollId){
        return scrollId >= 2049300 && scrollId <= 2049309; 
    }
    
    private static boolean isSpecialScroll(int scrollId){
        return scrollId >= 2530000 && scrollId <= 2530002 || scrollId == 2531000 || scrollId == 2532000 || scrollId == 2040727 || scrollId == 2041058;
    }
        
    private static boolean isGoodness(int scrollId){
        return scrollId == 2049122 || scrollId == 2049129 || scrollId == 2049130 || scrollId == 2049131 || scrollId == 2049148 || scrollId == 2049149 || scrollId == 2049135 || scrollId == 2049136 || scrollId == 2049137 || scrollId == 2049153;
    }
    
    private static boolean isMiraculous(int scrollId){
        return scrollId == 2049116 || scrollId == 2049132 || scrollId == 2049133 || scrollId == 2049134 || scrollId == 2049142 || scrollId == 2049150 || scrollId == 2049151 || scrollId == 2049152;
    }    
}
