/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.channel.handlers;

import client.MapleClient;
import client.inventory.MapleInventoryType;
import net.AbstractMaplePacketHandler;
import server.MapleInventoryManipulator;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Steven, even(RageZone), Zingzags(PokeCommunity)
 */
public class ItemSwitchExtenedBag extends AbstractMaplePacketHandler  {

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) { 
        slea.skip(4); // tick
        byte src = (byte) slea.readInt();                                       //01 00
        byte dst = (byte) slea.readInt();                                            //00 00
        if (src < 100 || dst < 100) {
            return;
        }
        MapleInventoryManipulator.move(c, MapleInventoryType.ETC, src, dst);
    }
}
