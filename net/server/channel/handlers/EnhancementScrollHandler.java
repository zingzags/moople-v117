/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.channel.handlers;

import client.MapleClient;
import net.AbstractMaplePacketHandler;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Steven, even(RageZone), Zingzags(PokeCommunity)
 */
public class EnhancementScrollHandler extends AbstractMaplePacketHandler {

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        slea.skip(4);
        ScrollHandler.scroll(c, (byte) slea.readShort(), (byte) slea.readShort(), (short) 0, 0);
    }
    
}
