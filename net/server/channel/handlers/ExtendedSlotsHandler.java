/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.channel.handlers;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.inventory.ModifyInventory;
import constants.ItemConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.AbstractMaplePacketHandler;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Steven, even(RageZone), Zingzags(PokeCommunity)
 */
public class ExtendedSlotsHandler extends AbstractMaplePacketHandler{

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
       MapleCharacter chr = c.getPlayer();   
       final List<ModifyInventory> mods = new ArrayList<>();
       boolean firstTime = false;
       
       if (chr == null || !chr.isAlive() || chr.getTrade() != null) {
           c.getSession().write(MaplePacketCreator.enableActions());
           return;
       }
        
       slea.readInt(); //tick
       short slot = slea.readShort();
       int itemid = slea.readInt();
       Item toUse = chr.getInventory(MapleInventoryType.ETC).getItem(slot);
        
       if (toUse == null || toUse.getQuantity() < 1 || toUse.getItemId() != itemid || itemid / 10000 != 433) {
           c.getSession().write(MaplePacketCreator.enableActions());
           return;
       }
               
       if(!chr.getExtendedSlots().contains(itemid)){//The first time for any of the bags
           firstTime = true;
       }  
       
       for(int i = 0; i < chr.getExtendedSlots().size(); i++){
           if(chr.getExtendedSlots().get(i) == itemid && i <= 2){ //To check if we already have the item
               
           }
       }
       
       if(chr.getExtendedSlots().size() > 3){
           firstTime = false;
       } else{
           firstTime = true;
       }
       
        if (firstTime) {
            chr.setExtendedSlots(itemid);
            toUse.setFlag((byte) ItemConstants.UNTRADEABLE);
            mods.add(new ModifyInventory(7, toUse));
            c.announce(MaplePacketCreator.modifyInventory(true, mods, chr));
            //c.announce(MaplePacketCreator.modifyInventory(firstTime, Collections.singletonList(new ModifyInventory(7, toUse)),chr));
        }
        c.announce(MaplePacketCreator.openExtendedBag(chr.getExtendedSlots().indexOf(itemid), itemid, firstTime));
        c.announce(MaplePacketCreator.enableActions());
        
    }
    
}
