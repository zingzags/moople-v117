/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.handlers.login;

/**
 *
 * @author Steven
 */
public enum WorldServerBackgroundHandler {
    
    Background1("e4", false),
    Background2("e3", false),
    Background3("e2", true),
    Background4("e1", true);
    
    
    private final String image;
    private final boolean flag;
    
    private WorldServerBackgroundHandler(String image, boolean flag) {
        this.image = image;
        this. flag = flag;
    }
    
    public String getImage() {
        return image;
    }
    
    public boolean getFlag() {
        return flag;
    }
}
