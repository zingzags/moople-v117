/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.handlers.login;

import client.MapleClient;
import net.AbstractMaplePacketHandler;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Steven
 */
public class RedirectorLoginHandler extends AbstractMaplePacketHandler {

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        c.setSendPing(true);
        c.login("admin", "admin");
        c.setAccountName("admin");
        c.finishLogin();
        c.announce(MaplePacketCreator.login());        
        c.announce(MaplePacketCreator.getAuthSuccess(c));        
    }
    
    public boolean validateState(MapleClient c) {
        return true;
    }    
}
