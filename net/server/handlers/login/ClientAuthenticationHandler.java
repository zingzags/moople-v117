/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.handlers.login;

import client.MapleClient;
import net.AbstractMaplePacketHandler;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Steven
 */
public class ClientAuthenticationHandler extends AbstractMaplePacketHandler {

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        int pRequest = slea.readInt();
        int pResponse;

        pResponse = ((pRequest >> 5) << 5) + (((((pRequest & 0x1F) >> 3) ^ 2) << 3) + (7 - (pRequest & 7)));
        pResponse |= ((pRequest >> 7) << 7);
        c.announce(MaplePacketCreator.getClientAuthentication(pResponse));
    }
    @Override
    public boolean validateState(MapleClient c) {
        return true;
    }    
}
