package net.server.handlers.login;

import client.MapleClient;
import java.net.InetAddress;
import java.net.UnknownHostException;
import net.AbstractMaplePacketHandler;
import net.server.Server;
import tools.FilePrinter;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

public class CharSelectedWithPicHandler extends AbstractMaplePacketHandler {

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        String pic = slea.readMapleAsciiString();
        int charId = slea.readInt();
        slea.readByte();
        String macs = slea.readMapleAsciiString();
        c.updateMacs(macs);

        if (c.hasBannedMac()) {
            c.getSession().close(true);
            return;
        }
        if (c.checkPic(pic)) {
            if (c.getIdleTask() != null) {
                c.getIdleTask().cancel(true);
            }
            c.updateLoginState(MapleClient.LOGIN_SERVER_TRANSITION);

            String[] socket = Server.getInstance().getIP(c.getWorld(), c.getChannel()).split(":");
            try {
                c.announce(MaplePacketCreator.getServerIP(InetAddress.getByName(socket[0]), Integer.parseInt(socket[1]), charId));
            } catch (Exception e) {
                FilePrinter.printError(FilePrinter.EXCEPTION_CAUGHT, e, "UNKOWN HOST EXCEPTION CAUGHT:");
            }
        } else {
            c.announce(MaplePacketCreator.wrongPic());
        }
    }    
}
