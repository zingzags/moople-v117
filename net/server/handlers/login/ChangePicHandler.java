/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.handlers.login;

import client.MapleClient;
import net.AbstractMaplePacketHandler;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Steven
 */
public class ChangePicHandler extends AbstractMaplePacketHandler {

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        if (c.getPic().length() > 0 || c.getPic() != null) {
            String oldPic = slea.readMapleAsciiString();
            String newPic = slea.readMapleAsciiString();
            if (newPic.contains(oldPic)) {
                c.announce(MaplePacketCreator.changePic((byte) 0x46));
            } else {
                c.setPic(newPic);
                c.announce(MaplePacketCreator.changePic((byte) 0));
            }
        }
    }
}
