/*
 This file is part of the OdinMS Maple Story Server
 Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
 Matthias Butz <matze@odinms.de>
 Jan Christian Meyer <vimes@odinms.de>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation version 3 as published by
 the Free Software Foundation. You may not use, modify or distribute
 this program under any other version of the GNU Affero General Public
 License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.server.handlers.login;

import client.MapleCharacter;
import client.MapleClient;
import client.MapleJob;
import client.MapleSkinColor;
import client.autoban.AutobanFactory;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.AbstractMaplePacketHandler;
import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;
import server.MapleItemInformationProvider;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

public final class CreateCharHandler extends AbstractMaplePacketHandler {
           
    private static List<Integer> allowedEquips = new ArrayList<>();
 
    public static void checkEquipXML(){
        final MapleDataProvider prov = MapleDataProviderFactory.getDataProvider(new File("wz/Etc.wz"));

        final MapleData infoData = prov.getData("MakeCharInfo.img");
        final MapleData data = infoData.getChildByPath("Info");
        for(int i = 0; i < 8; i++){
               
               final MapleData data2 = data.getChildByPath("CharMale/" + (i));
               final List <MapleData> data3 = data2.getChildren();
                 for (MapleData dat : data3) {
                        allowedEquips.add(MapleDataTool.getInt(dat));
                 }
                 
               final MapleData data4 = data.getChildByPath("CharFemale/" + (i));
               final List <MapleData> data5 = data4.getChildren();
                 for (MapleData dat : data5) {
                        allowedEquips.add(MapleDataTool.getInt(dat));
                 }                 
         }
    }
    
   
    private static boolean containsInt(List<Integer> array, int toCompare){
        for (int i = 0; i < array.size(); i++) {
             if (array.get(i) == toCompare){
                    return true;
             }      
        }
        return false;
    }         

    @Override
    public final void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        String name = slea.readMapleAsciiString();
        if (!MapleCharacter.canCreateChar(name)) {
            return;
        } 
        slea.readInt();
        slea.readInt();
        int job = slea.readInt();// BIGBANG: 0 = Resistance, 1 = Adventurer, 2 = Cygnus, 3 = Aran, 4 = Evan, 5 = mercedes, 8 = db
        MapleJob jobs = MapleJob.getById(job);
        short subJob = slea.readShort(); //0 = Adventurers, 1 = DualBlade, 2 = Cannon shooters
        byte gender = slea.readByte();
        byte skinColor = slea.readByte(); // 01
        int hairColor = 0;        
        byte unk2 = slea.readByte(); // 06
        int face = slea.readInt();
        int hair = slea.readInt();
        final boolean mercedes = (jobs == MapleJob.MERCEDES1);
        final boolean demon = (jobs == MapleJob.CITIZEN_DEMON_SLAYER);
        final boolean phantom = (jobs == MapleJob.PHANTOM1);
        final boolean mihile = (jobs == MapleJob.MIHILE);
        final boolean adventurer = (jobs == MapleJob.BEGINNER);
        boolean jettPhantom = (jobs == MapleJob.JETT_BEGINNER) || (jobs == MapleJob.PHANTOM_BEGINNER) || (jobs == MapleJob.BLADE_RECRUIT);
        
        if (!mercedes && !demon && !jettPhantom ) {
            if (!adventurer) {
                hairColor = slea.readInt();
            }
            skinColor = (byte) slea.readInt();
        }
           
        if (skinColor > 3) {
            return;
        }
         
        int demonMark = demon ? slea.readInt() : 0;        
        int top = slea.readInt();
        int bottom = (mercedes || demon || adventurer) ? 0 : slea.readInt();
        int cape;
    
        if  (phantom) {
            cape = slea.readInt();
        }
        int shoes = slea.readInt();
        int weapon = slea.readInt();
        int shield = 0;
        
        if (slea.available() >= 4){
            shield = slea.readInt();
        }
        
    /*    if(job == 1){      
            checkEquipXML();  
            if(!containsInt(allowedEquips, hair) && containsInt(allowedEquips, top) && containsInt(allowedEquips, bottom) && containsInt(allowedEquips, shoes) && containsInt(allowedEquips, weapon)){
                 AutobanFactory.FAKE_CHAR.autoban(c.getPlayer(), "Stupid Fucking Noob! Trying to packet edit!");
                 return;
            }
        }*/
        
        MapleCharacter newchar = MapleCharacter.getDefault(c);
               
        newchar.setWorld(c.getWorld());        
        newchar.setFace(face);
        newchar.setHair((job == 7) ? 36033 : hair + hairColor);  // Need to set Mihile's hair, or else it would be purple.
        newchar.setGender(gender);
        newchar.setName(name);
        newchar.setSkinColor(MapleSkinColor.getById(skinColor));        
        newchar.setMarkings(demonMark);
        newchar.setEars(mercedes);            
        newchar.setSubCategory(subJob);

        if (job == 0){
            newchar.setJob(MapleJob.CITIZEN);
            newchar.setMapId(931000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161054, (short) 0, (short) 1));             
        } else if (job == 1) {
            newchar.setJob(MapleJob.BEGINNER);
            newchar.setMapId(4000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));        
        } else if (job == 2) {
            newchar.setJob(MapleJob.NOBLESSE);
            newchar.setMapId(130030000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161047, (short) 0, (short) 1));
        } else if (job == 3) {
            newchar.setJob(MapleJob.LEGEND);
            newchar.setMapId(914000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161048, (short) 0, (short) 1));
        } else if (job == 4) {
            newchar.setJob(MapleJob.EVAN_BEGINNER);
            newchar.setMapId(900090000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161052, (short) 0, (short) 1));  
        } else if (job == 5) {
            newchar.setJob(MapleJob.MERCEDES_BEGINNER);
            newchar.setMapId(910150000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161079, (short) 0, (short) 1));  
        } else if (job == 6) {
            newchar.setJob(MapleJob.CITIZEN_DEMON_SLAYER);
            newchar.setMapId(931050310);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161054, (short) 0, (short) 1));            
        } else if (job == 7) {
            newchar.setJob(MapleJob.PHANTOM_BEGINNER);
            newchar.setMapId(10000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161048, (short) 0, (short) 1));       
        } else if (job == 8) {
            newchar.setJob(MapleJob.BLADE_RECRUIT);
            newchar.setMapId(103050900);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));   
        } else if (job == 9) {
            newchar.setJob(MapleJob.MIHILE);
            newchar.setMapId(913070000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161047, (short) 0, (short) 1));  
        } else if (job == 10) {
            newchar.setJob(MapleJob.LUMINOUS_BEGINNER);
            newchar.setMapId(101000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 11) {
            newchar.setJob(MapleJob.KAISER_BEGINNER);
            newchar.setMapId(101000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 12) {
            newchar.setJob(MapleJob.ANGELIC_BEGINNER);
            newchar.setMapId(940011000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 13) {
            newchar.setJob(MapleJob.CANNON_BEGINNER);
            newchar.setMapId(3000600);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 14) {
            newchar.setJob(MapleJob.LUMINOUS_BEGINNER);
            newchar.setMapId(101000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 15) {
            newchar.setJob(MapleJob.ZERO_BEGINNER);
            newchar.setMapId(321000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 16) {
            newchar.setJob(MapleJob.JETT_BEGINNER);
            newchar.setMapId(552000050);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 17) {
            newchar.setJob(MapleJob.HAYATO_BEGINNER);
            newchar.setMapId(807000000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 18) {
            newchar.setJob(MapleJob.KANNA_BEGINNER);
            newchar.setMapId(807040000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));    
        } else if (job == 19) {
            newchar.setJob(MapleJob.BEAST_BEGINNER);
            newchar.setMapId(866135000);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));
        } else if (job == 20) {
            newchar.setJob(MapleJob.SHADE_BEGINNER);
            newchar.setMapId(0);
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(4161001, (short) 0, (short) 1));      
        } else {
            c.announce(MaplePacketCreator.deleteCharResponse(0, 9));
            return;
        }
        //CHECK FOR EQUIPS
        //-1 Hat | -2 Face | -3 Eye acc | -4 Ear acc | -5 Overall | -6 bottom | -7 Shoes | -9 Cape | -10 Shield | -11 Weapon        
        MapleInventory equip = newchar.getInventory(MapleInventoryType.EQUIPPED);

        if (newchar.isGM()) {
            Item eq_hat = MapleItemInformationProvider.getInstance().getEquipById(1002140);
            eq_hat.setPosition((short) -1);
            equip.addFromDB(eq_hat);
          if(bottom > 0){  
            top = 1042003;
            bottom = 1062007;
          }
            weapon = 1322013;
        }
        
        Item eq_top = MapleItemInformationProvider.getInstance().getEquipById(top);
        eq_top.setPosition((short) -5);
        equip.addFromDB(eq_top);
        
        Item codex = MapleItemInformationProvider.getInstance().getEquipById(1172000);
        eq_top.setPosition((short) -55);
        equip.addFromDB(codex);        
        
        if (bottom > 0) {
            Item eq_bottom = MapleItemInformationProvider.getInstance().getEquipById(bottom);
            eq_bottom.setPosition((short) -6);
            equip.addFromDB(eq_bottom);
        }
        
        Item eq_shoes = MapleItemInformationProvider.getInstance().getEquipById(shoes);
        eq_shoes.setPosition((short) -7);
        equip.addFromDB(eq_shoes);
        
        if(shield > 0){
            Item eq_shield = MapleItemInformationProvider.getInstance().getEquipById(weapon);
            eq_shield.setPosition((short) -10);
            equip.addFromDB(eq_shield);     
        }
        
        Item eq_weapon = MapleItemInformationProvider.getInstance().getEquipById(weapon);
        eq_weapon.setPosition((short) -11);
        equip.addFromDB(eq_weapon.copy());
        
        if (!newchar.insertNewChar()) {
            c.announce(MaplePacketCreator.deleteCharResponse(0, 9));
            return;
        }
        c.announce(MaplePacketCreator.addNewCharEntry(newchar));
    }
}