/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.handlers.login;

import client.MapleClient;
import net.AbstractMaplePacketHandler;
import net.SendOpcode;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Steven, even(RageZone), Zingzags(PokeCommunity)
 */
public final class ClientErrorHandler extends AbstractMaplePacketHandler {
    
    @Override
    public final void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {

        if (slea.available() < 8) {
            return;
        }
        System.out.println("\nClient error: " + slea.toString());
        String error = "";
        short type = slea.readShort();
        if (type == 0x01) {
            error = "Wrong SendOp Code"; 
        } else if (type == 0x02) {
            error = "Crash Report";
        } else if (type == 0x03) {
            error = "Exception";
        }              

        int errortype = slea.readInt(); // example error 38
        if (errortype == 0) { // i don't wanna log error code 0 stuffs, (usually some bounceback to login)
            return;
        }
        short data_length = slea.readShort();
        slea.skip(4);
        short opcodeheader = slea.readShort();
        String opCodeName = "NON EXISTANT OPCODE";
        for(SendOpcode op : SendOpcode.values()){
            if(op.getValue() == opcodeheader){
                opCodeName = op.toString();
            }
        }
        System.out.println("Error type:" + error + "\nRecv opcode:" + opCodeName + " - 00x" + Integer.toHexString(opcodeheader) + "\nLength:" + data_length);     
    }  
    @Override
    public boolean validateState(MapleClient c) {
        return true;
    }    
}
